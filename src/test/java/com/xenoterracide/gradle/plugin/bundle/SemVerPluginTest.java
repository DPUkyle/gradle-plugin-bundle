/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import com.xenoterracide.gradle.plugin.SemVerPlugin;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.revwalk.RevCommit;
import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SemVerPluginTest {

    private Project project;

    @BeforeEach
    void setup() throws Exception {
        project = ProjectBuilder.builder().build();

        String msg1 = "one";
        String msg2 = "two";
        String msg3 = "three";

        Git git = new InitCommand().setDirectory( project.getProjectDir() ).call();
        RevCommit one = git.commit().setMessage( msg1 ).setAllowEmpty( true ).call();
        RevCommit two = git.commit().setMessage( msg2 ).setAllowEmpty( true ).call();
        RevCommit three = git.commit().setMessage( msg3 ).setAllowEmpty( true ).call();

        git.tag().setAnnotated( true ).setMessage( msg1 ).setName( "v0.1.1" ).setObjectId( one ).call();
        git.tag().setAnnotated( true ).setMessage( msg2 ).setName( "v0.1.2" ).setObjectId( two ).call();
        git.tag().setAnnotated( true ).setMessage( msg3 ).setName( "v0.1.3" ).setObjectId( three ).call();

    }


    @Test
    void apply() {
        project.getPluginManager().apply( SemVerPlugin.class );
        assertEquals( "0.1.3", project.getVersion() );
    }

}