/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import com.github.spotbugs.SpotBugsExtension;
import com.github.spotbugs.SpotBugsPlugin;
import net.ltgt.gradle.errorprone.ErrorPronePlugin;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.revwalk.RevCommit;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaLibraryPlugin;
import org.gradle.api.plugins.PluginContainer;
import org.gradle.api.plugins.quality.CheckstyleExtension;
import org.gradle.api.plugins.quality.CheckstylePlugin;
import org.gradle.plugins.ide.idea.IdeaPlugin;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance( TestInstance.Lifecycle.PER_CLASS )
class XenoPluginTest {

    private Project project;

    @BeforeEach
    void setup() throws Exception {
        project = ProjectBuilder.builder().build();
        Git git = new InitCommand().setDirectory( project.getProjectDir() ).call();
        RevCommit one = git.commit().setMessage( "one" ).setAllowEmpty( true ).call();
    }


    @ParameterizedTest
    @ArgumentsSource( PluginProvider.class )
    void codeQuality( Collection<Class<Plugin<Project>>> pluginClass ) {
        pluginClass.forEach( project.getPluginManager()::apply );

        PluginContainer plugins = project.getPlugins();

        assertAll( "plugins",
                   () -> assertTrue( plugins.hasPlugin( IdeaPlugin.class ), "idea" ),
                   () -> assertTrue( plugins.hasPlugin( SpotBugsPlugin.class ), "spotbugs" ),
                   () -> assertTrue( plugins.hasPlugin( ErrorPronePlugin.class ), "errorprone" ),
                   () -> assertTrue( plugins.hasPlugin( CheckstylePlugin.class ), "checkstyle" ),
                   () -> assertTrue( plugins.hasPlugin( JavaLibraryPlugin.class ), "java" )
        );

        assertThat( project.getExtensions().getByType( SpotBugsExtension.class ).getSourceSets() ).isNotEmpty();
        assertThat( project.getExtensions().getByType( CheckstyleExtension.class ).getSourceSets() ).isNotEmpty();
    }

    static class PluginProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments( ExtensionContext context ) {
            return Stream.of(
                    Arrays.asList( JavaLibraryPlugin.class, XenoCodeQualityPlugin.class ),
                    Collections.singleton( JavaProjectDefaultsPlugin.class )
            ).map( Arguments::of );
        }
    }
}
