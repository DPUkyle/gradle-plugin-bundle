/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import io.spring.gradle.dependencymanagement.DependencyManagementPlugin;
import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.concurrent.TimeUnit;

import static com.xenoterracide.gradle.plugin.DepConstants.APT;
import static com.xenoterracide.gradle.plugin.DepConstants.COMPILE;
import static com.xenoterracide.gradle.plugin.DepConstants.IMPL;
import static com.xenoterracide.gradle.plugin.DepConstants.LATEST;
import static com.xenoterracide.gradle.plugin.DepConstants.TEST_COMPILE;
import static com.xenoterracide.gradle.plugin.DepConstants.TEST_IMPL;
import static com.xenoterracide.gradle.plugin.Dependencies.AssertJ;
import static com.xenoterracide.gradle.plugin.Dependencies.D;
import static com.xenoterracide.gradle.plugin.Dependencies.EqualsVerifier;

public class DependencyManagement implements Plugin<Project> {
    @Override
    public void apply( Project project ) {
        project.getConfigurations().all( conf -> {
            conf.resolutionStrategy( rs -> rs.cacheChangingModulesFor( 1, TimeUnit.MINUTES ) );
        } );
        project.getPluginManager().apply( DependencyManagementPlugin.class );
        project.getExtensions().configure( DependencyManagementExtension.class, ext -> {
            ext.imports( handler -> {
                handler.mavenBom( "org.springframework.boot:spring-boot-starter-parent:2.0.6.RELEASE" );
            } );
        } );

        project.getDependencies().constraints( dch -> {
            dch.add( TEST_IMPL, String.join( D, AssertJ.G, AssertJ.A, AssertJ.V ) );
            dch.add( TEST_IMPL, String.join( D, EqualsVerifier.G, EqualsVerifier.A, EqualsVerifier.V ) );
            dch.add( COMPILE, "org.immutables:value:2.+" );
            dch.add( IMPL, "com.google.guava:guava:" + LATEST );
            dch.add( APT, "org.immutables:value:2.+" );
            dch.add( APT, "org.immutables:builder:2.+" );
            dch.add( TEST_COMPILE, "org.immutables:value:2.+" );
        } );

    }
}
