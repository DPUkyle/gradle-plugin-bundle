/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.quality.Checkstyle;
import org.gradle.api.plugins.quality.CheckstyleExtension;
import org.gradle.api.plugins.quality.CheckstylePlugin;

public class CheckStyle implements Plugin<Project> {

    @Override
    public void apply( Project project ) {
        project.getPluginManager().apply( CheckstylePlugin.class );
        project.getExtensions().getByType( CheckstyleExtension.class ).setToolVersion( "8.10.1" );
        project.getTasks().withType( Checkstyle.class ).configureEach( task -> {
            task.getReports().getHtml().setEnabled( false );
            task.getReports().getXml().setEnabled( false );
            task.setShowViolations( true );
        } );
    }
}
