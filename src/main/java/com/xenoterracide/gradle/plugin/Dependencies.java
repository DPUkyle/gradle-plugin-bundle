/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

final class Dependencies {
    private static final String DELIMETER = ":";
    static final String D = DELIMETER;

    static class Junit {
        static final String G = "org.junit.jupiter";

        static class Api {
            static final String A = "junit-jupiter-api";
        }

        static class Engine {
            static final String A = "junit-jupiter-engine";
        }

        static class Param {
            static final String A = "junit-jupiter-params";
        }
    }

    static class AssertJ {
        static final String G = "org.assertj";
        static final String A = "assertj-core";
        static final String V = "[3.8,4)";
    }

    static class EqualsVerifier {
        static final String G = "nl.jqno.equalsverifier";
        static final String A = "equalsverifier";
        static final String V = "2.+";
    }
}
