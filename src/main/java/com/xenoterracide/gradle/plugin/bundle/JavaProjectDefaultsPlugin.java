/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import com.xenoterracide.gradle.plugin.DependencyManagement;
import com.xenoterracide.gradle.plugin.JavaDefaultsPlugin;
import com.xenoterracide.gradle.plugin.MavenPublishing;
import com.xenoterracide.gradle.plugin.MavenRepositories;
import com.xenoterracide.gradle.plugin.SemVerPlugin;
import com.xenoterracide.gradle.plugin.TestSuiteConfigPlugin;
import org.gradle.api.Plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class JavaProjectDefaultsPlugin extends XenoCodeQualityPlugin {
    @Override
    Collection<Class<? extends Plugin<?>>> plugins() {
        List<Class<? extends Plugin<?>>> classes = new ArrayList<>( super.plugins() );
        Collections.addAll(
                classes,
                MavenRepositories.class,
                JavaDefaultsPlugin.class,
                TestSuiteConfigPlugin.class,
                DependencyManagement.class,
                SemVerPlugin.class,
                MavenPublishing.class
        );
        return classes;
    }
}
