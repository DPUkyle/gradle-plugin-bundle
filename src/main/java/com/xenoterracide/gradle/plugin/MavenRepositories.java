/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import org.apache.commons.lang.StringUtils;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.RepositoryHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

public class MavenRepositories implements Plugin<Project> {
    private static final String ENV = "JAR_REPOSITORY_URI";
    private final Logger log = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void apply( Project project ) {
        String uri = System.getenv( ENV );
        log.info( ENV + "={}", uri );
        RepositoryHandler repoHandler = project.getRepositories();
        if ( StringUtils.isNotBlank( uri ) ) {
            repoHandler.maven( repo -> repo.setUrl( URI.create( uri ) ) );
        }
        repoHandler.mavenCentral();
    }
}
